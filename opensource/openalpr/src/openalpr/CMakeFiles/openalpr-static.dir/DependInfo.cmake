# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/mnt/c/aiknow/openalpr-master/src/openalpr/cjson.c" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/cjson.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "DEFAULT_CONFIG_FILE=\"/etc/openalpr/openalpr.conf\""
  "INSTALL_PREFIX=\"/usr/local\""
  "OPENALPR_MAJOR_VERSION=2"
  "OPENALPR_MINOR_VERSION=3"
  "OPENALPR_PATCH_VERSION=0"
  "OPENCV_MAJOR_VERSION=3"
  "SKIP_STATE_DETECTION=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/tesseract"
  "/usr/include/opencv"
  "./openalpr"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/mnt/c/aiknow/openalpr-master/src/openalpr/alpr.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/alpr.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/alpr_c.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/alpr_c.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/alpr_impl.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/alpr_impl.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/binarize_wolf.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/binarize_wolf.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/colorfilter.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/colorfilter.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/config.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/config.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/config_helper.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/config_helper.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/detection/detector.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/detection/detector.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/detection/detectorcpu.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/detection/detectorcpu.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/detection/detectorcuda.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/detection/detectorcuda.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/detection/detectorfactory.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/detection/detectorfactory.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/detection/detectormask.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/detection/detectormask.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/detection/detectormorph.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/detection/detectormorph.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/detection/detectorocl.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/detection/detectorocl.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/edges/edgefinder.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/edges/edgefinder.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/edges/platecorners.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/edges/platecorners.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/edges/platelines.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/edges/platelines.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/edges/scorekeeper.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/edges/scorekeeper.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/edges/textlinecollection.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/edges/textlinecollection.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/licenseplatecandidate.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/licenseplatecandidate.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/motiondetector.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/motiondetector.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/ocr/ocr.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/ocr/ocr.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/ocr/ocrfactory.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/ocr/ocrfactory.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/ocr/segmentation/charactersegmenter.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/ocr/segmentation/charactersegmenter.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/ocr/segmentation/histogram.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/ocr/segmentation/histogram.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/ocr/segmentation/histogramhorizontal.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/ocr/segmentation/histogramhorizontal.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/ocr/segmentation/histogramvertical.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/ocr/segmentation/histogramvertical.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/ocr/tesseract_ocr.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/ocr/tesseract_ocr.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/pipeline_data.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/pipeline_data.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/postprocess/postprocess.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/postprocess/postprocess.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/postprocess/regexrule.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/postprocess/regexrule.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/prewarp.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/prewarp.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/result_aggregator.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/result_aggregator.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/textdetection/characteranalysis.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/textdetection/characteranalysis.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/textdetection/linefinder.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/textdetection/linefinder.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/textdetection/platemask.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/textdetection/platemask.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/textdetection/textcontours.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/textdetection/textcontours.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/textdetection/textline.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/textdetection/textline.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/transformation.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/transformation.cpp.o"
  "/mnt/c/aiknow/openalpr-master/src/openalpr/utility.cpp" "/mnt/c/aiknow/openalpr-master/src/openalpr/CMakeFiles/openalpr-static.dir/utility.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DEFAULT_CONFIG_FILE=\"/etc/openalpr/openalpr.conf\""
  "INSTALL_PREFIX=\"/usr/local\""
  "OPENALPR_MAJOR_VERSION=2"
  "OPENALPR_MINOR_VERSION=3"
  "OPENALPR_PATCH_VERSION=0"
  "OPENCV_MAJOR_VERSION=3"
  "SKIP_STATE_DETECTION=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/tesseract"
  "/usr/include/opencv"
  "./openalpr"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
