import sys
from alprstream import AlprStream
from openalpr import Alpr

alpr = Alpr("us", "/mnt/c/aiknow/openalpr-master/config/openalpr.conf.defaults", "/mnt/c/aiknow/openalpr-master/runtime_data/")
if not alpr.is_loaded():
    print("Error loading OpenALPR")
    sys.exit(1)
alpr_stream = AlprStream(frame_queue_size=10, use_motion_detection=True)
if not alpr_stream.is_loaded():
    print("Error loading AlprStream")
    sys.exit(1)

alpr_stream.connect_video_file('/mnt/c/aiknow/720p.mp4', 0)

while alpr_stream.video_file_active() or alpr_stream.get_queue_size() > 0:
    single_frame = alpr_stream.process_frame(alpr)
    active_groups = len(alpr_stream.peek_active_groups())
    print("Active groups: {:<3} \tQueue size: {}".format(active_groups, alpr_stream.get_queue_size()))
    groups = alpr_stream.pop_completed_groups()
    for group in groups:
        print("=" * 50)
        print("Group ({}-{}): {} ({:.2f}% confident)".format(
            group['epoch_start'], group['epoch_end'],
            group['best_plate']['plate'], group['best_plate']['confidence']))
        print("=" * 50)
