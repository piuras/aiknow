import sys
import os
import cv2
import numpy
#from openalpr import Alpr
from lib.plates.openalpr import Alpr
from argparse import ArgumentParser

parser = ArgumentParser(description='OpenALPR Python Test Program')

parser.add_argument("-c", "--country", dest="country", action="store", default="us",
                  help="License plate Country" )

parser.add_argument("--config", dest="config", action="store", default="/etc/openalpr/openalpr.conf",
                  help="Path to openalpr.conf config file" )

parser.add_argument("--runtime_data", dest="runtime_data", action="store", default="/usr/share/openalpr/runtime_data",
                  help="Path to OpenALPR runtime_data directory" )

parser.add_argument('plate_image', help='License plate image file')

options = parser.parse_args()


alpr = None
alpr = Alpr(options.country, options.config, options.runtime_data)

if not alpr.is_loaded():
	print("Error loading OpenALPR")
else:
	print("Using OpenALPR " + alpr.get_version())

alpr.set_top_n(7)

vidcap = cv2.VideoCapture(options.plate_image)
count = 0
success = True

while success:
	success,image = vidcap.read()
	success2,image_png=cv2.imencode('.png', image)
	print("Read a new frame[" + str(count) + "]:" + str(success) + "::" + str(success2))
	results = alpr.recognize_ndarray(image_png)
	cv2.imwrite("tmp/frame%d.png" % count, image_png)  

	# Uncomment to see the full results structure
	# import pprint
	# pprint.pprint(results)
	count += 1

	if len(results['results'])==0:
		continue

	print("Image size: %dx%d" %(results['img_width'], results['img_height']))
	print("Processing Time: %f" % results['processing_time_ms'])

	i = 0
	for plate in results['results']:
	    i += 1
	    print("Plate #%d" % i)
	    print("   %12s %12s" % ("Plate", "Confidence"))
	    for candidate in plate['candidates']:
		prefix = "-"
		if candidate['matches_template']:
		    prefix = "*"

		print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))

