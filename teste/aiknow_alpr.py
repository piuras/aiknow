#!/usr/bin/python

import 	sys
from 	argparse 			import ArgumentParser
from 	lib.plates.openalpr 		import Alpr
from 	lib.video.videostream  		import WebcamVideoStream
from 	lib.config.configs 		import Configuracoes as cfg

parser = ArgumentParser(description='Aiknow - OpenALPR Python Test Program')
parser.add_argument('video', help='License plate video file')

options = parser.parse_args()

alpr = Alpr(cfg.country, cfg.alpr_conf , cfg.alpr_data)

vs = WebcamVideoStream(src=options.video ).start()

i = 0
while True:
	print("frame [%d]..." % i)
	frame = vs.read()
	results = alpr.recognize_ndarray(frame)
    	i += 1

	if len(results['results'])==0:
		continue
	j = 0
	for plate in results['results']:
		j += 1
		for candidate in plate['candidates']:
			prefix = "-"
			if candidate['matches_template']:
				prefix = "*"
		print("%s" % (candidate['plate']))
