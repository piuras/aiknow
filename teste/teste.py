import sys
from openalpr import Alpr

# alpr = Alpr("us", "./arquivos/config/us.conf", "/usr/share/openalpr/runtime_data/")
alpr = Alpr("eu", "/mnt/c/aiknow/openalpr-master/config/openalpr.conf.defaults", "/mnt/c/aiknow/openalpr-master/runtime_data/")
if not alpr.is_loaded():
    print("Error loading OpenALPR")
    sys.exit(1)
    
alpr.set_top_n(5)
#alpr.set_default_region("md")

# results = alpr.recognize_file("/mnt/c/aiknow/train-detector-master/br/pl_ZRX2978.png")
results = alpr.recognize_file("1.png")
print("tamanho #%d" % len(results['results']))
print(results)
if len(results['results'])==0:
	sys.exit(1)

i = 0
for plate in results['results']:
    i += 1
    print("Plate #%d" % i)
    print("   %12s %12s" % ("Plate", "Confidence"))
    for candidate in plate['candidates']:
        prefix = "-"
        if candidate['matches_template']:
            prefix = "*"

        print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))

# Call when completely done to release memory
#alpr.unload()
