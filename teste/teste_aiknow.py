import sys
from openalpr import Alpr
from videostream import WebcamVideoStream

# alpr = Alpr("us", "./arquivos/config/us.conf", "/usr/share/openalpr/runtime_data/")
alpr = Alpr("eu", "/mnt/c/aiknow/openalpr-master/config/openalpr.conf.defaults", "/mnt/c/aiknow/openalpr-master/runtime_data/")
if not alpr.is_loaded():
    print("Error loading OpenALPR")
    sys.exit(1)
    
alpr.set_top_n(5)
#alpr.set_default_region("md")

# results = alpr.recognize_file("/mnt/c/aiknow/train-detector-master/br/pl_ZRX2978.png")
#vs = WebcamVideoStream(src='720p.mp4').start()
vs = WebcamVideoStream(src='/mnt/c/aiknow/gravacao_Placas_Carro.mp4').start()
i = 0
while True:
    	i += 1
	print("frame [%d]..." % i)
	frame = vs.read()
	#results = alpr.recognize_file(frame)
	results = alpr.recognize_ndarray(frame)
	i = 0
	for plate in results['results']:
		i += 1
		print("Plate #%d" % i)
		print("   %12s %12s" % ("Plate", "Confidence"))
		for candidate in plate['candidates']:
			prefix = "-"
			if candidate['matches_template']:
				prefix = "*"

		print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))

print("tamanho #%d" % len(results['results']))
        #if alpr.is_loaded():
        #	alpr.unload()

print(results)
if len(results['results'])==0:
	sys.exit(1)

i = 0
for plate in results['results']:
    i += 1
    print("Plate #%d" % i)
    print("   %12s %12s" % ("Plate", "Confidence"))
    for candidate in plate['candidates']:
        prefix = "-"
        if candidate['matches_template']:
            prefix = "*"

        print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))

# Call when completely done to release memory
#alpr.unload()
