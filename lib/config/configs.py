class Configuracoes:
	enableVideoWindow = True      # Habilita window no linux para visualizar o que esta acontecendo True/False
	enableFullScreenVideo = True  # Habilita window full screen somente se enableVideoWindow = True | ESC fecha janela | True/False
	currentResolution = (1366,768)
	enableShowFPS = False
	showWindowScaleFactor = 0.5
	enableWaterMark = True
	videoSource = 0
	videoWidth = 1280
	videoHeight = 720
	videoFps = 30
	resizeScale = 0.4
	bBoxWPadding = 20
	bBoxHPadding = 20
	lbpDefaultScaleFactor = 1.04
	lbpminNeighbors = 7
	haarDefaultScaleFactor = 1.04
	haarminNeighbors = 7
	trackerDistanceThreshold = 160
	trackerMaxFrameSkip = 10
	trackerTraceLenght = 50
	trackIdCount = 100
        codec = "MPG4"
        #codec = 1196444237.0 # MJPG
	country = "eu"
	alpr_conf = "config/openalpr.conf.defaults"
	alpr_data = "data"
