# import the necessary packages
import 	cv2
from 	threading 		import Thread
from 	lib.config.configs	import Configuracoes as cfg

class WebcamVideoStream:
    def __init__(self, src=0, name="WebcamVideoStream"):
        global cfg

        # initialize the video camera stream and read the first frame
        # from the stream
        self.stream = cv2.VideoCapture(src)
        #print 'fourcc:', decode_fourcc(codec)
        #self.stream.set(cv2.CAP_PROP_FOURCC, cfg.codec)
        self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*cfg.codec))
        self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, cfg.videoWidth)
        self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, cfg.videoHeight)
        self.stream.set(cv2.CAP_PROP_FPS, cfg.videoFps)    
        #print("\t Width: ", self.stream.get(cv2.CAP_PROP_FRAME_WIDTH))
        #print("\t Height: ", self.stream.get(cv2.CAP_PROP_FRAME_HEIGHT))
        #print("\t FourCC: ", self.stream.get(cv2.CAP_PROP_FOURCC))
        (self.grabbed, self.frame) = self.stream.read()

        # initialize the thread name
        self.name = name

        # initialize the variable used to indicate if the thread should
        # be stopped
        self.stopped = False

    def start(self):
        # start the thread to read frames from the video stream
        t = Thread(target=self.update, name=self.name, args=())
        t.daemon = True
        t.start()
        return self

    def update(self):
        # keep looping infinitely until the thread is stopped
        while True:
            # if the thread indicator variable is set, stop the thread
            if self.stopped:
                return

            # otherwise, read the next frame from the stream
            (self.grabbed, self.frame) = self.stream.read()

    def read(self):
        # return the frame most recently read
        return self.frame

    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True
