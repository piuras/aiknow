#!/usr/bin/python

import 	sys
import 	cv2
from 	argparse 			import ArgumentParser
from 	lib.plates.openalpr 		import Alpr
from 	lib.video.videostream  		import WebcamVideoStream
from 	lib.config.configs 		import Configuracoes as cfg

parser = ArgumentParser(description='Aiknow - OpenALPR Python Test Program')
parser.add_argument('video', help='License plate video file')
parser.add_argument("-o", "--output", dest="output", action="store", default="tmp", help="Output path" )

options = parser.parse_args()

alpr = Alpr(cfg.country, cfg.alpr_conf , cfg.alpr_data)

vs = WebcamVideoStream(src=options.video ).start()

i = 0
while True:
	frame = vs.read()[100:1080, 0:1920]
	results = alpr.recognize_ndarray(frame)
    	i += 1

	if len(results['results'])==0:
		continue
	j = 0
	for plate in results['results']:
		plate_ok=plate['candidates'].pop(0)['plate']
		print("frame[%d]::candidate[%d]::plate[%s]" % (i-1, j, plate_ok))
		cv2.imwrite("%s/%d_%d_%s.png" % (options.output, i-1, j, plate_ok), frame)  
		j += 1
